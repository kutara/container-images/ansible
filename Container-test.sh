#!/usr/bin/env bash

set -x

echo running tests ensure to cleanup containers

export YUM_CACHE=/tmp/

bash -e ./Container.sh

CONTAINER=$(buildah containers --format "{{.ContainerName}}")
buildah commit "$CONTAINER" ansible
buildah rm --all
