#!/usr/bin/env bash

set -x

export IMAGE=registry.fedoraproject.org/fedora-minimal@sha256:5a6ca4d2b2923ab84f10601b3b1d5c6246586de8b0bdbf1b94dd59de473fb68a

# renovate: datasource=repology depName=fedora_34/python-dns versioning=loose
export PY_DNS_VERSION=2.1.0
# renovate: datasource=repology depName=fedora_34/ansible versioning=loose
export ANSIBLE_VERSION=2.9.21

ctr1=$(buildah from --arch "$ARCH" "$IMAGE")

buildah run -v "$YUM_CACHE":/var/cache/yum:O "$ctr1" bash -c "
microdnf -y update \
&& microdnf -y install \
ansible-$ANSIBLE_VERSION \
python-dns \
&& adduser ansible
"

buildah config --user ansible "$ctr1"

buildah config \
--label org.opencontainers.image.version=$ANSIBLE_VERSION \
--label org.opencontainers.image.title=Ansible \
--label org.opencontainers.image.description="Ansible is a radically simple model-driven configuration management, multi-node deployment, and remote task execution system." \
--label org.opencontainers.image.url=https://ansible.com \
--label org.opencontainers.image.documentation=https://docs.ansible.com \
--label org.opencontainers.image.vendor="Red Hat" \
--label org.opencontainers.image.licenses=GPL-3.0 \
--label org.opencontainers.image.source="https://gitlab.com/kutara/container-images/ansible" \
--label io.containers.autoupdate=registry \
--label io.kutara.version.ansible=$ANSIBLE_VERSION \
"$ctr1"


buildah config --cmd '[ "ansible" ]' "$ctr1"
